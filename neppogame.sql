create database testelogin;
use testelogin;


CREATE TABLE funcionarios (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    senha VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    saldo INT,
    telefone VARCHAR(20)
);

select * from funcionarios;

DROP TABLE funcionarios;

