

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class registrarServlet
 */
@WebServlet("/registrarServlet")
public class registrarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("nome");	
		String email = request.getParameter("email");	
		String senha = request.getParameter("senha");
		String tel = request.getParameter("tel");
		String nulo = null;
		try {
			//conexão com o banco
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/testelogin", "root", "");
			Statement stmt = con.createStatement();
			// query de insert no banco
			PreparedStatement ps = con.prepareStatement("INSERT INTO funcionarios (nome, senha, email, telefone) VALUES(?, ?,?,?)");
			ps.setString(1, nome);
			ps.setString(2, senha);
			ps.setString(3, email);
			ps.setString(4, tel);
			ps.executeUpdate();
			ps.close();
			con.close();
			response.sendRedirect("./index.html");
		}catch (Exception p){
			System.out.println(p);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
