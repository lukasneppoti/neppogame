

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Pegando parametros do html
		PrintWriter out = response.getWriter();
		String usuario = request.getParameter("email");	
		String senha = request.getParameter("senha");
		HttpSession session = request.getSession();
		boolean flag = false;
		// colocando driver de conexao mysql
		
		try {
			//conexão com o banco
			Class.forName("com.mysql.jdbc.Driver");
			//endereco do banco localhost/nomedobanco usuario senha
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/testelogin", "root", "");
			Statement stmt = con.createStatement();
			// query de seleção no banco
			ResultSet rs = 	stmt.executeQuery("select * from funcionarios");
			while(rs.next()) {
				// condição caso for verdadeiro(procura se os usuario e senha são compativeis)
				if(usuario.equals(rs.getString(4)) && senha.equals(rs.getString(3))){
					session.setAttribute("user", usuario);
					flag = true;
					response.sendRedirect("./Servlet2");
				}
			}
			if(flag==false) {
				out.print("Usuario ou senha invalido.");
			}
		}catch (Exception p){
			out.print(p);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
